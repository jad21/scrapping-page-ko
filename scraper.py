#!/usr/bin/env python
# -*- coding: latin-1 -*-


# author:jad21
# email: esojangel@gmail.com
# whatsapp: +584120309959


import os
import sys

BASE = os.path.dirname(os.path.abspath(__file__))
site_packages = os.path.join(BASE, 'libs', 'site-packages')
sys.path.append(site_packages)

import aiohttp
import asyncio
import lxml.html
from lxml import etree
import hashlib
import time
import glob
import re


PATH_DATA = "data"
BINARY_PHANTOMJS = os.path.join(BASE, 'phantomjs.exe')
# BINARY_PHANTOMJS = "/home/jdelgado/phantomjs-2.1.1-linux-x86_64/bin/phantomjs"
CACHEABLE = False

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
    'Accept': 'image/webp,image/*,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, sdch',
    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
    # 'Referer':url,
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
}

PAGE_INDEX_TEMPLATE = b"""
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="content-language" content="kr">
        <meta property="og:locale" content="ko_KR">
        <link href="../css/css1.css" rel="stylesheet" type="text/css">
        <link href="../css/css2.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="dgn_wrap">
            <div id="dgn_gallery_wrap">
                <div id="   ">
                    <div id="dgn_content_de">%s</div>
                </div>
            </div>
        </div>
    </body>
"""


class http():
    session_http = None

    @staticmethod
    def _b_url(url):
        return hashlib.md5(url.encode('utf-8')).hexdigest()

    @staticmethod
    async def get(url, **karg):
        if CACHEABLE:
            url_b = os.path.join(PATH_DATA, "requests", http._b_url(url))
            if os.path.exists(url_b):
                print(url_b)
                with open(url_b, "rb") as f:
                    return 200, f.read()
        # headers["Referer"] = url
        session = http.session_http
        res = await session.get(url, **karg, headers=session.headers)
        status = res.status
        content = await res.read()
        if CACHEABLE and status == 200:
            with open(url_b, "wb") as f:
                f.write(content)
        return status, content

    @staticmethod
    async def post(url, **karg):
        session = http.session_http
        session.headers['X-Requested-With'] = "XMLHttpRequest"
        res = await session.post(url, **karg, headers=session.headers)
        status = res.status
        content = await res.read()
        return status, content


class Scrape():

    userDefault = "stock_new2"
    startUrl = 'http://gall.dcinside.com/board/lists/?id=%s&page=%s&exception_mode=recommend'
    pageUrl = 'http://gall.dcinside.com/board/view/?id=%s&no=%s&page=1&exception_mode=recommend'
    pagecomment = 'http://gall.dcinside.com/comment/view'  # method POST
    is_outputs = {
        "page": False,
        "jpg": False,
        "index": False,
        "screen": False,
        "url_page": False,
        "url_image": False,
        "text": False,
        "title": False,
        "replies": False,
    }
    real_outputs = {
        "page": "page.html",
        "jpg": "*.jpp",
        "index": "index.html",
        "screen": "screen.png",
        "url_page": "url.txt",
        "url_image": "ImageURL*.txt",
        "text": "Text*.txt",
        "title": "Title.txt",
        "replies": "Replies.txt",
    }

    is_final_page = False

    def set_outputs(self, is_outputs=""):
        for k, v in self.is_outputs.items():
            if k in is_outputs:
                self.is_outputs[k] = True

    async def handler(self):
        for page in range(9999):
            try:
                await self.parse_page(page)
            except Exception as e:
                # print(e)
                continue

    async def parse_page(self, page_id):
        url = self.startUrl % (self.userDefault, str(page_id))

        status_code, html = await http.get(url)

        if status_code != 200:
            raise Exception("Error to page response", status_code)
        page = lxml.html.document_fromstring(html)
        items = page.cssselect('td.t_notice')
        parser_ids = []

        sem_id = asyncio.Semaphore(3)
        for item in items:
            try:
                n = int(item.text)
            except:
                continue
            parser_ids.append(self.parser_ids_with_semaphore(sem_id, item.text))
            # if len(parser_ids) == 1:
            #     break

        await asyncio.gather(*parser_ids)
        # #important
        # nextPage = page.cssselect('a[class="b_next"]')
        # if len(nextPage) == 0:
        #     print("")

    async def parser_ids_with_semaphore(self, sem, id):
        async with sem:
            for x in range(10):
                try:
                    return await self.parse_by_id(id)
                except Exception as e:
                    await asyncio.sleep(1)
                    return await self.parse_by_id(id)
        return None

    async def parse_by_id(self, id):
        path = os.path.join(PATH_DATA, self.userDefault, id)
        if glob.glob(path + " -*"):
            return
        else:
            if not os.path.exists(path):
                os.makedirs(path)

        sys.stdout.write("Download : %s\n" % (id))
        url = self.pageUrl % (self.userDefault, id)

        # cmd = '%s download1.js %s "%s" >> log.txt' % (BINARY_PHANTOMJS, id, url)
        # print(cmd)
        # os.system(cmd)
        ''' download page '''
        status_code, html_content = await http.get(url)
        with open(os.path.join(path, 'page.html'), "wb") as f:
            f.write(html_content)

        with open(os.path.join(path, 'page.html'), "rb") as f:
            html = f.read().decode()

        page = lxml.html.document_fromstring(html)
        content = page.cssselect('div.re_gall_box_1')[0]

        imgs_list, text_list = self.get_data_content(content)
        ''' download imgs '''
        if self.is_outputs["jpg"] or self.is_outputs["url_image"] or self.is_outputs["text"]:
            sys.stdout.write("Download : jpg \n")
            n = 0

            http.session_http.headers['Referer'] = url

            if self.is_outputs["text"]:
                sep_txt = 0
                for txt in text_list:
                    sep_txt += 1
                    path_txt = os.path.join(path, 'Text%s.txt' % str(sep_txt))
                    with open(path_txt, 'wb') as f:
                        f.write(txt.encode())

            for pic in content.cssselect('img'):
                n += 1

                if self.is_outputs["jpg"] or self.is_outputs["url_image"]:
                    img_src = pic.get('src')
                    try:
                        # print(img_src)
                        # await http.download_coroutine(img_src)
                        status_code, r_img = await http.get(img_src)
                        if status_code != 200:
                            continue
                    except Exception as e:
                        # print("error")
                        # print(e)
                        continue
                    if self.is_outputs["jpg"]:
                        name_img = "Image" + str(n) + '.jpg'
                        path_img = os.path.join(path, name_img)
                        with open(path_img, 'wb') as f:
                            f.write(r_img)
                        pic.attrib['src'] = name_img

                    if self.is_outputs["url_image"]:
                        path_img = os.path.join(path, 'ImageURL%s.txt' % str(n))
                        with open(path_img, 'w') as f:
                            f.write(img_src)

        subjectItem = page.cssselect('div[class="w_top_left"]')
        if self.is_outputs["index"]:
            content = lxml.html.etree.tostring(content, encoding='UTF-8', method='html', pretty_print=True)
            # dl[class="wt_subject"]')
            subject = lxml.html.etree.tostring(subjectItem[0], encoding='UTF-8', method='html', pretty_print=True)

            comments = page.cssselect('table[class="gallery_re_contents"]')
            if len(comments) > 0:
                comments = lxml.html.etree.tostring(comments[0], encoding='UTF-8', method='html', pretty_print=True)
            else:
                comments = ''

            with open(os.path.join(path, 'index.html'), "wb") as f:
                body = b"".join([subject, content, comments])
                index_page = PAGE_INDEX_TEMPLATE % (body)
                f.write(index_page)

            ''' download render pages '''
            if self.is_outputs["screen"]:
                sys.stdout.write("Download : screen \n")
                cmd = '%s render.js "%s" ' % (BINARY_PHANTOMJS, os.path.join(self.userDefault, id))
                os.system(cmd)

        if self.is_outputs["url_page"]:
            with open(os.path.join(path, 'url.txt'), "w") as f:
                f.write(url)

        titleItem = subjectItem[0].cssselect('dl.wt_subject dd')
        title = ''
        if len(titleItem) > 0:
            title = titleItem[0].text

        if self.is_outputs["title"]:
            with open(os.path.join(path, 'Title.txt'), "wb") as f:
                f.write(title.encode())

        if self.is_outputs["replies"]:
            try:
                ci_t = page.cssselect('[name="ci_t"]')
                if ci_t:
                    ci_t = ci_t[0].value
                    await self.download_replies(ci_t=ci_t, id=self.userDefault, no=id, page=1, path=path)
                else:
                    with open(os.path.join(path, 'Replies.txt'), "wb+") as replies_file:
                        replies_file.write(b"")
            except Exception as e:
                print("We could not download the replies", e)
                raise e

        ''' del not selected'''
        for k, b in self.is_outputs.items():
            if not b:
                # print(k, b)
                for f in glob.glob(os.path.join(path, self.real_outputs[k])):
                    # print(f)
                    os.remove(f)

        try:
            regex = r"[\\\/:*?\"<>|,\']"
            title = re.sub(regex, "", title, 0, re.IGNORECASE)
            final_name = path + ' - ' + title
            os.rename(path, final_name)
        except Exception as e:
            sys.stdout.write("\nRename error for %s\n" % id)
            print(e)

        sys.stdout.write("done\n")
        sys.stdout.flush()
        return True

    async def download_replies(self, **kargs):
        params = dict(
            ci_t=kargs.get("ci_t"),
            id=kargs.get("id"),
            no=kargs.get("no")
        )
        comment_page = int(kargs.get("comment_page", 1))
        params["comment_page"] = comment_page
        with open(os.path.join(kargs.get("path"), 'Replies.txt'), "wb+") as replies_file:
            replies_file.write(b"")
            while True:
                print(params)

                status_code, html = await http.post(self.pagecomment, data=params)
                params["comment_page"] += 1
                if not html:
                    break
                if status_code == 200:

                    page = lxml.html.document_fromstring(html.decode())
                    for row in page.xpath('//tr[@class="reply_line"]'):
                        try:
                            username = row.xpath("td[@user_name]")[0].text_content().strip().ljust(10).encode()
                            retime = row.cssselect('td[class="retime"]')[0].text_content().strip().ljust(25).encode()
                            reply = row.cssselect('td[class="reply"]')[0].text_content().strip().encode()
                            replies_file.write(b"Date: %s User: %s Reply: %s\r\n" % (retime, username, reply))
                        except Exception as e:
                            continue

    async def get_with_semaphore(self, page=1):
        async with self.sem:
            return await self.parse_page(page)

    def get_data_content(self, content):
        text_list = list()
        imgs_list = list()
        nodes_td = content.xpath("//table/tr/td")
        # print(nodes_td[0].xpath("*[not(self::br)]"))
        # exit()
        sep_img = 0
        sep_txt = 0
        sep_txt_use = False
        for node in nodes_td:
            if node.xpath("*[not(self::br)]"):
                for p in node.xpath("*[not(self::br)]"):
                    if p.xpath("img"):
                        imgs_list.append(p.xpath("img"))
                        sep_img += 1
                        if sep_txt_use:
                            sep_txt += 1
                            sep_txt_use = False
                    elif p.text_content():
                        sep_txt_use = True
                        # print(sep_txt, range(0, len(text_list)))
                        if "- dc official app" in str(p.text_content()).lower() or "- dc official app" == str(p.text_content()).lower():
                            continue
                        if sep_txt in range(0, len(text_list)):
                            text_list[sep_txt] = text_list[sep_txt] + "\n" + p.text_content()
                        else:
                            text_list.append(p.text_content())
            else:
                text_list.append(node.text_content())

            # if node.getnext
        # print(imgs_list, text_list)
        # exit()
        return imgs_list, text_list


def requirements():
    if not os.path.exists(PATH_DATA):
        os.makedirs(PATH_DATA)
    if not os.path.exists(os.path.join(PATH_DATA, 'requests')):
        os.makedirs(os.path.join(PATH_DATA, 'requests'))


class View():
    """docstring for View"""
    userDefault = "stock_new2"

    def input_user(self):
        user = input("Enter user [%s]:" % (self.userDefault))
        if user:
            self.userDefault = user

    def menu(self):
        o_default = "jpg,index,page,url_page,screen,url_image,text,title,replies"
        # o_default = "url_image"
        outputs_num = input("Outputs [%s]:" % (o_default))
        if not outputs_num:
            outputs_num = o_default
        return ",".join([opc.strip() for opc in outputs_num.split(",")])

    async def run_async(self):
        async with aiohttp.ClientSession() as session:
            session.headers = headers
            http.session_http = session
            scraper = Scrape()

            self.input_user()
            scraper.userDefault = self.userDefault
            outputs = self.menu()
            scraper.set_outputs(outputs)
            await scraper.handler()

            # scraper.set_outputs("replies,index")
            # scraper.userDefault = "baseball_new2"
            # await scraper.parse_by_id("9516826")

            # 1527737  1527384  1527068

            # await scraper.parse_by_id("1039872")
            # await scraper.parse_by_id("1528684")
            # await scraper.parse_by_id("750812")
            # scraper.set_outputs("index,page,title,jpg,url_image,text,url")
            # await scraper.parse_by_id("708946")
            # await scraper.parse_by_id("1530321")

            # webtoon&no=1530321

            # stock_new2&no=935214
            # scraper.userDefault = "stock_new2"
            # scraper.set_outputs("index,page,title,jpg,url_image,text,url")
            # await scraper.parse_by_id("935214")
            # await scraper.parse_by_id("939316")
            # await scraper.parse_by_id("937022")

    def run(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.run_async())
        loop.close()
        print("end.")
        time.sleep(10)


if __name__ == '__main__':
    requirements()
    View().run()
